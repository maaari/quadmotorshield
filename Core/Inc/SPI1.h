/*
 * SPI1.h
 *
 *  Created on: Feb 20, 2022
 *      Author: User
 */

#ifndef INC_SPI1_H_
#define INC_SPI1_H_

void SPI1_TransmitReceiveSlaveDMA(uint8_t *bufOut, uint8_t *bufIn, int bufLen);
void initSPI1(void);
uint16_t MY_ISP1_RXBUF_GetData(int *shift);


#endif /* INC_SPI1_H_ */
