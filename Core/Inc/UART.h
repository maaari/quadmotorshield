/*
 * UART.h
 *
 *  Created on: Jan 26, 2022
 *      Author: User
 */
#include "main.h"
#ifndef INC_UART_H_
#define INC_UART_H_

void uartDMA_Init(void);
void UART_RX(uint8_t *buf, uint16_t sz);
void Out485(uint8_t *buf, int size);
uint16_t UART_RXBUF_GetData(uint16_t *shift);

#endif /* INC_UART_H_ */
