/*
 * command_task.h
 *
 *  Created on: 16 февр. 2022 г.
 *      Author: User
 */

#ifndef SRC_COMMAND_TASK_H_
#define SRC_COMMAND_TASK_H_
#include "main.h"

void command_task(uint8_t *buf, uint8_t UART_SPI);
#endif /* SRC_COMMAND_TASK_H_ */
