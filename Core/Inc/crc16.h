/*
 * crc16.h
 *
 *  (c) 29    . 2015  . apotorochin
 */

#ifndef CRC16_H_
#define CRC16_H_
#include "main.h"
//#include "ring_buffer.h"

//                                                    
//           CRC16

uint16_t crc16Calculate (uint8_t *data, uint16_t lenght);
//uint16_t crc16InRingBufferCalculate(RingBuffer *buf, uint16_t lenght, uint16_t firstSymbolNumber);


#endif /* CRC16_H_ */
