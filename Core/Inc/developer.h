/*
 * developer.h
 *
 *  Created on: 16 февр. 2022 г.
 *      Author: User
 */

#ifndef SRC_DEVELOPER_H_
#define SRC_DEVELOPER_H_

#define COMMAND_MODE	(0)
#define FIRST_REGISTER	(1)
#define COINT_REGISTER	(2)



#define READ_REGISTER	(1)
#define WRITE_REGISTER	(2)

void developer(uint8_t *buf, int len);
void developerUSART(uint8_t *buf, int len);
void developerSPI(uint8_t *buf, uint8_t len);

#endif /* SRC_DEVELOPER_H_ */
