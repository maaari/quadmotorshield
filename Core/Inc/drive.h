/*
 * drive.h
 *
 *  Created on:
 *      Author: User
 */

#ifndef INC_DRIVE_H_
#define INC_DRIVE_H_
#include "main.h"
#include "lptim.h"
#include "tim.h"

#define MOTOR_MODE_STOP     (0)
#define MOTOR_MODE_PWM      (1)
#define MOTOR_MODE_SPEED    (2)

#define DEFAULT_SPEED_PERIOD (10)
#define DEFAULT_PID_P (20)
#define DEFAULT_PID_I (3)
#define DEFAULT_PID_D (0)

typedef struct
{
	int32_t prev_err;
	int16_t ierr;
	int32_t I;
}PidData_t;

extern uint8_t flag_tim;
extern PidData_t pid1, pid2, pid3, pid4;
extern int16_t enc1, enc2,enc3,enc4;
//extern int32_t old_speed1, new_speed1;
//extern int32_t old_speed2, new_speed2;
//extern int32_t old_speed3, new_speed3;
//extern int32_t old_speed4, new_speed4;

void driver_task(void);
void drive_init(void);
void speed_to_sec(void);
void mode_task(void);

#endif /* INC_DRIVE_H_ */
