/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2022 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32l0xx_hal.h"

#include "stm32l0xx_ll_dma.h"
#include "stm32l0xx_ll_spi.h"
#include "stm32l0xx_ll_usart.h"
#include "stm32l0xx_ll_rcc.h"
#include "stm32l0xx_ll_bus.h"
#include "stm32l0xx_ll_cortex.h"
#include "stm32l0xx_ll_system.h"
#include "stm32l0xx_ll_utils.h"
#include "stm32l0xx_ll_pwr.h"
#include "stm32l0xx_ll_gpio.h"

#include "stm32l0xx_ll_exti.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define DIR1_Pin GPIO_PIN_13
#define DIR1_GPIO_Port GPIOC
#define STM_MISO_EN_Pin GPIO_PIN_14
#define STM_MISO_EN_GPIO_Port GPIOC
#define ADC_Pin GPIO_PIN_0
#define ADC_GPIO_Port GPIOA
#define B2_Pin GPIO_PIN_1
#define B2_GPIO_Port GPIOA
#define A3_Pin GPIO_PIN_2
#define A3_GPIO_Port GPIOA
#define B3_Pin GPIO_PIN_3
#define B3_GPIO_Port GPIOA
#define STM_SPI_NSS_Pin GPIO_PIN_4
#define STM_SPI_NSS_GPIO_Port GPIOA
#define A4_Pin GPIO_PIN_6
#define A4_GPIO_Port GPIOA
#define B4_Pin GPIO_PIN_7
#define B4_GPIO_Port GPIOA
#define PWM3_Pin GPIO_PIN_0
#define PWM3_GPIO_Port GPIOB
#define PWM4_Pin GPIO_PIN_1
#define PWM4_GPIO_Port GPIOB
#define STM_EN5V_Pin GPIO_PIN_2
#define STM_EN5V_GPIO_Port GPIOB
#define STM_CON_PWR_Pin GPIO_PIN_12
#define STM_CON_PWR_GPIO_Port GPIOB
#define DIR2_Pin GPIO_PIN_13
#define DIR2_GPIO_Port GPIOB
#define DIR3_Pin GPIO_PIN_14
#define DIR3_GPIO_Port GPIOB
#define DIR4_Pin GPIO_PIN_15
#define DIR4_GPIO_Port GPIOB
#define BREAK_COAST_Pin GPIO_PIN_8
#define BREAK_COAST_GPIO_Port GPIOA
#define STM_TX_Pin GPIO_PIN_9
#define STM_TX_GPIO_Port GPIOA
#define STM_RX_Pin GPIO_PIN_10
#define STM_RX_GPIO_Port GPIOA
#define A2_Pin GPIO_PIN_15
#define A2_GPIO_Port GPIOA
#define STM_DIR_Pin GPIO_PIN_3
#define STM_DIR_GPIO_Port GPIOB
#define PWM1_Pin GPIO_PIN_4
#define PWM1_GPIO_Port GPIOB
#define DIVIDER_EN_Pin GPIO_PIN_6
#define DIVIDER_EN_GPIO_Port GPIOB
#define B1_Pin GPIO_PIN_7
#define B1_GPIO_Port GPIOB
#define STM_CON_PCHG_Pin GPIO_PIN_8
#define STM_CON_PCHG_GPIO_Port GPIOB
/* USER CODE BEGIN Private defines */
#define BUFSIZE 64
#define MARKER 0xC0
#define MYID 0x31
#define CMDUART 0
#define CMDSPI  1
/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */
