/*
 * ram_rom_data.h
 *
 */

#ifndef INC_RAM_ROM_DATA_H_
#define INC_RAM_ROM_DATA_H_

#include "main.h"

//RW
#define MOTOR_MODE                 (0)

#define M1_GOAL_SPEED              (1)
#define M2_GOAL_SPEED              (2)
#define M3_GOAL_SPEED              (3)
#define M4_GOAL_SPEED              (4)

#define PID_P                      (5)
#define PID_I                      (6)
#define PID_D                      (7)
//RO
#define M1_SPEED_sek               (8)
#define M2_SPEED_sek               (9)
#define M3_SPEED_sek               (10)
#define M4_SPEED_sek               (11)

#define M1_POWER                   (12)
#define M2_POWER                   (13)
#define M3_POWER                   (14)
#define M4_POWER                   (15)

#define M1_PRESENT_SPEED           (16)
#define M2_PRESENT_SPEED           (17)
#define M3_PRESENT_SPEED           (18)
#define M4_PRESENT_SPEED           (19)

#define M1_ENCODER                 (20)
#define M2_ENCODER                 (21)
#define M3_ENCODER                 (22)
#define M4_ENCODER                 (23)



#define RW_DATA_SIZE 	(5)
#define RO_DATA_SIZE 	(19)
#define DATA_SIZE (RW_DATA_SIZE + RO_DATA_SIZE)




extern int16_t ramRomData[];

#endif /* INC_RAM_ROM_DATA_H_ */
