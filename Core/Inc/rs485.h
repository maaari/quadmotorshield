/*
 * rs485.h
 *
 *  Created on: 16 февр. 2022 г.
 *      Author: User
 */

#ifndef INC_RS485_H_
#define INC_RS485_H_
#include "main.h"
uint16_t UART_RXBUF_GetData(uint16_t *shift);
void rs485Out1(uint8_t *buf, uint8_t dlina);

#endif /* INC_RS485_H_ */
