/*
 * SPI1.c
 *
 *  Created on: Feb 20, 2022
 *      Author: User
 */

#include "main.h"
#include <string.h>
#include <stdio.h>
extern uint8_t bufSPI_Tx[], bufSPI_Rx[];
//extern uint8_t bufSPI_Tx[], bufSPI_Rx[];
extern uint16_t SPI_RxBufLastCNDTR;
extern uint8_t bufCmd[], bufAns[], nBufCmd, nBufAns;
char sp[100];
void SPI1_TransmitReceiveSlaveDMA(uint8_t *bufOut, uint8_t *bufIn, int bufLen) {
	LL_DMA_DisableChannel(DMA1, LL_DMA_CHANNEL_3); // Tx
	LL_DMA_DisableChannel(DMA1, LL_DMA_CHANNEL_2); // Rx
	LL_DMA_SetDataLength(DMA1, LL_DMA_CHANNEL_3, bufLen);
	LL_DMA_SetDataLength(DMA1, LL_DMA_CHANNEL_2, bufLen);
	LL_DMA_ConfigAddresses(DMA1, LL_DMA_CHANNEL_3, (uint32_t) bufOut,
			LL_SPI_DMA_GetRegAddr(SPI1),
			LL_DMA_GetDataTransferDirection(DMA1, LL_DMA_CHANNEL_3));
	LL_DMA_ConfigAddresses(DMA1, LL_DMA_CHANNEL_2, LL_SPI_DMA_GetRegAddr(SPI1),
			(uint32_t) bufIn,
			LL_DMA_GetDataTransferDirection(DMA1, LL_DMA_CHANNEL_2));
	LL_DMA_EnableChannel(DMA1, LL_DMA_CHANNEL_2);
	LL_DMA_EnableChannel(DMA1, LL_DMA_CHANNEL_3);

}
//------------------------------------------------------------
void initSPI1(void) {
// Init of SPI1 Slave DMA Circular
	LL_DMA_DisableChannel(DMA1, LL_DMA_CHANNEL_2);
	LL_DMA_DisableChannel(DMA1, LL_DMA_CHANNEL_3);
	LL_DMA_ClearFlag_TC2(DMA1);
	LL_DMA_ClearFlag_TE2(DMA1);
	LL_DMA_ClearFlag_TC3(DMA1);
	LL_DMA_ClearFlag_TE3(DMA1);
	LL_SPI_EnableDMAReq_TX(SPI1);
	LL_SPI_EnableDMAReq_RX(SPI1);
	LL_DMA_EnableIT_TC(DMA1, LL_DMA_CHANNEL_2);
	LL_DMA_EnableIT_TE(DMA1, LL_DMA_CHANNEL_2);
	LL_DMA_EnableIT_TC(DMA1, LL_DMA_CHANNEL_3);
	LL_DMA_EnableIT_TE(DMA1, LL_DMA_CHANNEL_3);
	LL_SPI_Enable(SPI1);

	LL_mDelay(100);
	SPI_RxBufLastCNDTR = BUFSIZE;
	LL_SPI_Enable(SPI2);
}
//-----------------------------------------------------------
uint16_t MY_ISP1_RXBUF_GetData(int *shift) {
	uint32_t cndrt = DMA1_Channel2->CNDTR;
	*shift = BUFSIZE - SPI_RxBufLastCNDTR;

	if (SPI_RxBufLastCNDTR > cndrt) {
		uint16_t len = SPI_RxBufLastCNDTR - cndrt;
		SPI_RxBufLastCNDTR = cndrt;
		return len;
	} else if (SPI_RxBufLastCNDTR < cndrt) {
		uint16_t len = SPI_RxBufLastCNDTR;
		SPI_RxBufLastCNDTR = BUFSIZE;
		return len;
	}
	return 0;
}
//---------------------------------------------------------
void MY_ISP1_RXBUF_PutData(uint8_t *buf, uint16_t dlina) {
	int s = dlina > BUFSIZE ? BUFSIZE : dlina;

	memcpy(bufSPI_Tx, buf, s);
}
//-----------------------------------------------------------

