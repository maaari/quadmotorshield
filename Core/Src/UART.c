/*
 * UART.c
 *
 *  Created on: Jan 26, 2022
 *      Author: Marina
 */
#include "main.h"
#include "usart.h"
#include "dma.h"
#include <stdio.h>
#include "string.h"

//DMA_TypeDef d;
extern uint32_t UART_RxBufLastCNDTR;

void uartDMA_Init(void) {
	LL_DMA_DisableChannel(DMA1, LL_DMA_CHANNEL_4);
	LL_DMA_DisableChannel(DMA1, LL_DMA_CHANNEL_5);
	LL_DMA_ClearFlag_TC4(DMA1);
	LL_DMA_ClearFlag_TE4(DMA1);
	LL_DMA_ClearFlag_TC5(DMA1);
	LL_DMA_ClearFlag_TE5(DMA1);

	LL_USART_EnableDMAReq_RX(USART1);
	LL_USART_EnableDMAReq_TX(USART1);

	LL_DMA_EnableIT_TC(DMA1, LL_DMA_CHANNEL_4);
	LL_DMA_EnableIT_TE(DMA1, LL_DMA_CHANNEL_4);
	LL_DMA_EnableIT_TC(DMA1, LL_DMA_CHANNEL_5);
	LL_DMA_EnableIT_TE(DMA1, LL_DMA_CHANNEL_5);

	LL_DMA_ClearFlag_GI5(DMA1);
	LL_DMA_ClearFlag_GI4(DMA1);
	LL_DMA_ClearFlag_TC4(DMA1);
	LL_DMA_ClearFlag_TE4(DMA1);
	LL_DMA_ClearFlag_TC5(DMA1);
	LL_DMA_ClearFlag_TE5(DMA1);

	UART_RxBufLastCNDTR = BUFSIZE;

//	LL_DMA_ConfigAddresses(DMA1, LL_DMA_CHANNEL_5, LL_USART_DMA_GetRegAddr(USART1), (uint32_t)&inBufUart, LL_DMA_GetDataTransferDirection(DMA1, LL_DMA_CHANNEL_5));
//  LL_DMA_ConfigAddresses(DMA1, LL_DMA_CHANNEL_4, (uint32_t)out, LL_USART_DMA_GetRegAddr(USART1), LL_DMA_GetDataTransferDirection(DMA1, LL_DMA_CHANNEL_4));
}
//---------------------------------------------------------------------------
void UART_TX(uint8_t *out, uint16_t sz) {
	LL_DMA_DisableChannel(DMA1, LL_DMA_CHANNEL_4);
	LL_DMA_ConfigAddresses(DMA1, LL_DMA_CHANNEL_4, (uint32_t) out,
			LL_USART_DMA_GetRegAddr(USART1 ,LL_USART_DMA_REG_DATA_TRANSMIT),
			LL_DMA_GetDataTransferDirection(DMA1, LL_DMA_CHANNEL_4));
	LL_DMA_SetDataLength(DMA1, LL_DMA_CHANNEL_4, sz);
	LL_DMA_EnableChannel(DMA1, LL_DMA_CHANNEL_4);
}
//---------------------------------------------------------------------
void UART_RX(uint8_t *buf, uint16_t sz) {
	LL_DMA_DisableChannel(DMA1, LL_DMA_CHANNEL_5);
	LL_DMA_ConfigAddresses(DMA1, LL_DMA_CHANNEL_5,
			LL_USART_DMA_GetRegAddr(USART1,LL_USART_DMA_REG_DATA_RECEIVE), (uint32_t) buf,
			LL_DMA_GetDataTransferDirection(DMA1, LL_DMA_CHANNEL_5));
	LL_DMA_SetDataLength(DMA1, LL_DMA_CHANNEL_5, sz);
	LL_DMA_EnableChannel(DMA1, LL_DMA_CHANNEL_5);
}
//--------------------------------------------------------------------
uint16_t UART_RXBUF_GetData(uint16_t *shift) {
	uint32_t cndrt = DMA1_Channel5->CNDTR;
	*shift = BUFSIZE - UART_RxBufLastCNDTR;

	if (UART_RxBufLastCNDTR > cndrt) {
		uint16_t len = UART_RxBufLastCNDTR - cndrt;
		UART_RxBufLastCNDTR = cndrt;
		return len;
	} else if (UART_RxBufLastCNDTR < cndrt) {
		uint16_t len = UART_RxBufLastCNDTR;
		UART_RxBufLastCNDTR = BUFSIZE;
		return len;
	}
	return 0;
}

//void HAL_UART_TxCpltCallback(UART_HandleTypeDef *huart)
//{
//        // завершена передача всех данных
//	HAL_GPIO_WritePin(STM_DIR_GPIO_Port,STM_DIR_Pin,GPIO_PIN_RESET); // ВКЛЮЧЕНИЕ ПРИЁМА ПО RS-485
//	huart->gState = HAL_UART_STATE_READY;
//}
void DMA1_RS485_TransmitComplete(void)
{
//	LL_mDelay(1);
	while((USART1->ISR & USART_ISR_TC) == 0){};
	HAL_GPIO_WritePin(STM_DIR_GPIO_Port,STM_DIR_Pin,GPIO_PIN_RESET); // ВКЛЮЧЕНИЕ ПРИЁМА ПО RS-485
}
void Out485(uint8_t *buf, uint16_t size)
{
	  HAL_GPIO_WritePin(STM_DIR_GPIO_Port,STM_DIR_Pin,GPIO_PIN_SET);
	  UART_TX((uint8_t *)buf,size);
//	  HAL_UART_Transmit_DMA(&huart1,(uint8_t *)buf,size);
}
//uint16_t UART_RXBUF_GetData(uint16_t *shift)
//{
//	uint32_t cndrt = DMA1_Channel5->CNDTR;
//  *shift =  BUFSIZE - UART_RxBufLastCNDTR;
//
//  if(UART_RxBufLastCNDTR > cndrt)
//  {
//    uint16_t len = UART_RxBufLastCNDTR - cndrt;
//    UART_RxBufLastCNDTR = cndrt;
//	return len;
//  }
//	else if(UART_RxBufLastCNDTR < cndrt)
//  {
//		uint16_t len = UART_RxBufLastCNDTR;
//		UART_RxBufLastCNDTR = BUFSIZE;
//		return len;
//  }
//	return 0;
//}
