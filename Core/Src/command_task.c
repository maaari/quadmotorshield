/*
 * command_task.c
 *
 *  Created on: 16 февр. 2022 г.
 *      Author: User
 */
#include "main.h"
#include "developer.h"
#include "crc16.h"
// Удалить после завершения отладки
//#include <stdio.h>
//#include <string.h>
//void Out485(uint8_t *buf, uint16_t size);
//------------------------------------------

void command_task(uint8_t *buf, uint8_t UART_SPI)
{
	{// Check CRC16
		uint16_t crc16;

		crc16 = crc16Calculate(buf+2, buf[1]+2); // == 0 if OK
		//if(crc16) return;// crc != 0 ERROR
	    if(buf[2] != MYID) return;
		{

//			developer(buf+3, buf[1] - 1); // Обработка принятой команды

// Подготовка ответа на команду и ответ
			if(UART_SPI == CMDUART)
			{// Command from USART
				developerUSART(buf+3, buf[1] - 1);
			}
			else
			{// Command from SPI
				developerSPI(buf+3, buf[1] - 1);
			}

		}

	}
}
