/*
 * developer.c
 *
 *
 *      Author: User
 */

#include "main.h"
#include "UART.h"
#include "mySPI.h"
#include "developer.h"
#include "ram_rom_data.h"
#include "crc16.h"
#include "drive.h"
// Удалить после завершения отладки
#include <stdio.h>
#include <string.h>

static uint8_t bbuf[50];
void developer(uint8_t *buf, int len)
{



}
void developerUSART(uint8_t *buf, int len)
{
//	memcpy(bbuf,buf,len);
//	Out485(bbuf, len);
	uint8_t out_len;
	uint16_t crc16;

	bbuf[0] = MARKER;
	bbuf[2] = MYID;

	switch(buf[0])
	{
	case READ_REGISTER:
		out_len = buf[COINT_REGISTER]*2;
		bbuf[1] = out_len + 1;
		if((buf[COINT_REGISTER] + buf[FIRST_REGISTER]) > DATA_SIZE) return;
		memcpy(bbuf+3,ramRomData+buf[FIRST_REGISTER],out_len);
		crc16 = crc16Calculate(bbuf+2, bbuf[1]);
		bbuf[out_len + 3] = crc16 >> 8;
		bbuf[out_len + 4] = crc16;
		Out485(bbuf, out_len+5);
		break;

	case WRITE_REGISTER:
		if((buf[COINT_REGISTER] + buf[FIRST_REGISTER]) >= RW_DATA_SIZE) return;
		out_len = buf[COINT_REGISTER]*2;
		memcpy(ramRomData+buf[FIRST_REGISTER],buf+3,out_len);
		mode_task();

		break;

	}

}
//-------------------------------------------------
void developerSPI(uint8_t *buf, uint8_t len)
{

}
