/*
 * drive.c
 *
 *  Created on:
 *      Author: User
 */
#include "drive.h"
#include <stdint.h>
#include <stdbool.h>
#include "ram_rom_data.h"
#include <stdio.h>
#include "rs485.h"
//typedef struct
//{
//	int16_t prev_err;
//	int16_t ierr;
//}PidData_t;

//флаг для энкодера
uint8_t flag_tim=0;

static int16_t M1_buf_summ_speed[100];
static int16_t M2_buf_summ_speed[100];
static int16_t M3_buf_summ_speed[100];
static int16_t M4_buf_summ_speed[100];

uint8_t n_count_speed=0;

uint32_t tiktik = 0;
int16_t enc1, enc2,enc3,enc4;

//PidData_t pid1, pid2, pid3, pid4;

PidData_t pid1, pid2, pid3, pid4;

void drive_init(void)
{

	ramRomData[PID_P] = DEFAULT_PID_P;
	ramRomData[PID_I] = DEFAULT_PID_I;
	ramRomData[PID_D] = DEFAULT_PID_D;


	ramRomData[MOTOR_MODE] = 0;
	ramRomData[M1_POWER] = 0;
	ramRomData[M2_POWER] = 0;
	ramRomData[M3_POWER] = 0;
	ramRomData[M4_POWER] = 0;

	ramRomData[M1_GOAL_SPEED] = 0;
	ramRomData[M2_GOAL_SPEED] = 0;
	ramRomData[M3_GOAL_SPEED] = 0;
	ramRomData[M4_GOAL_SPEED] = 0;
}

void speed_to_sec(void)
{
	int32_t result_speed1=0, result_speed2=0, result_speed3=0, result_speed4 = 0;
	for (int i = 0; i < 100; i++)
	{
		result_speed1 += M1_buf_summ_speed[i];
		result_speed2 += M2_buf_summ_speed[i];
		result_speed3 += M3_buf_summ_speed[i];
		result_speed4 += M4_buf_summ_speed[i];
	}
	ramRomData[M1_SPEED_sek] = result_speed1 * 20;
	ramRomData[M2_SPEED_sek] = result_speed2 * 20;
	ramRomData[M3_SPEED_sek] = result_speed3 * 20;
	ramRomData[M4_SPEED_sek] = result_speed4 * 20;

}//void speed_to_sek(void)

void encoder_update(void)
{
	int16_t speed = 0;


//	enc1 = LPTIM1->CNT << 1;
//	enc2 = TIM2->CNT;
//	enc3 = TIM21->CNT;
//	enc4 = TIM22->CNT;

	speed =  (ramRomData[M1_ENCODER] - enc1);
	ramRomData[M1_ENCODER] = enc1;
	ramRomData[M1_PRESENT_SPEED] = speed;
	M1_buf_summ_speed[n_count_speed] = speed;


	speed =  (ramRomData[M2_ENCODER] - enc2);
	ramRomData[M2_ENCODER] = enc2;
	ramRomData[M2_PRESENT_SPEED] = speed;
	M2_buf_summ_speed[n_count_speed] = speed;


	speed =  (ramRomData[M3_ENCODER] - enc3);
	ramRomData[M3_ENCODER] = enc3;
	ramRomData[M3_PRESENT_SPEED] = speed;
	M3_buf_summ_speed[n_count_speed] = speed;

	speed =  (ramRomData[M4_ENCODER] - enc4);
	ramRomData[M4_ENCODER] = enc4;
	ramRomData[M4_PRESENT_SPEED] = speed;
    M4_buf_summ_speed[n_count_speed] = speed;


	n_count_speed ++;
	if (n_count_speed > 100) n_count_speed = 0;


}//encoder_update(void)

static void pwm_mode_task(void)
{
	TIM3->CCR1 = ramRomData[M1_POWER];
	TIM3->CCR3 = ramRomData[M3_POWER]; //Motor 3
	TIM3->CCR4 = ramRomData[M4_POWER]; //Motor 4
	  //TIM2->CCR3 = 0;// Motor 2
}

int16_t pid_update(PidData_t* pid, int16_t present_speed, int16_t goal_speed)
{
	int32_t pwm=0;
	int16_t serr;
	int32_t power_limit = 1000;
	serr = (goal_speed/20 - (present_speed*100));


	int32_t P = (ramRomData[PID_P]*serr)/100;
	int32_t I = (pid->I + (ramRomData[PID_I]*serr))/100;
	int32_t D = ramRomData[PID_D]*(serr - pid->prev_err)/100;
	pwm = (P + I + D);
//
	if(pwm > power_limit)
		pwm = power_limit;
	else if (pwm < -power_limit)
		pwm = -power_limit;
//	else if(pwm < -power_limit)
//	else if(pwm < 0)
//		pwm = -pwm;

	pid->I = I;
	pid->prev_err = serr;


	return pwm;


}//void pid_update(PidData_t* pid, int32_t present, int32_t goal)

void speed_task(void)
{
	int16_t present_speed, goal_speed;
	int16_t pwm;
	int16_t result_pwm = 0;

	present_speed = ramRomData[M1_PRESENT_SPEED];
	goal_speed = ramRomData[M1_GOAL_SPEED];
	if ((present_speed <= 0) && (goal_speed <0))
	{
		present_speed = -present_speed;
		goal_speed = -goal_speed;
	}
	pwm = pid_update(&pid1, present_speed, goal_speed);
	result_pwm = pwm + ramRomData[M1_POWER];
	if (result_pwm > TIM3->ARR) result_pwm = TIM3->ARR;
	ramRomData[M1_POWER] = result_pwm;


	present_speed = ramRomData[M2_PRESENT_SPEED];
	goal_speed = ramRomData[M2_GOAL_SPEED];
	if ((present_speed <= 0) && (goal_speed <0))
	{
		present_speed = -present_speed;
		goal_speed = -goal_speed;
	}
	pwm = pid_update(&pid2, present_speed, goal_speed);
	result_pwm = pwm + ramRomData[M2_POWER];
	if (result_pwm > TIM3->ARR) result_pwm = TIM3->ARR;
	ramRomData[M2_POWER] = result_pwm;


	present_speed = ramRomData[M3_PRESENT_SPEED];
	goal_speed = ramRomData[M3_GOAL_SPEED];
	if ((present_speed <= 0) && (goal_speed <0))
	{
		present_speed = -present_speed;
		goal_speed = -goal_speed;
	}
	pwm = pid_update(&pid3, present_speed, goal_speed);
	result_pwm = pwm + ramRomData[M3_POWER];
	if (result_pwm > TIM3->ARR) result_pwm = TIM3->ARR;
	ramRomData[M3_POWER] = result_pwm;

	present_speed = ramRomData[M4_PRESENT_SPEED];
	goal_speed = ramRomData[M4_GOAL_SPEED];
	if ((present_speed <= 0) && (goal_speed <0))
	{
		present_speed = -present_speed;
		goal_speed = -goal_speed;
	}
	pwm = pid_update(&pid4, present_speed, goal_speed);
	result_pwm = pwm + ramRomData[M4_POWER];
	if (result_pwm > TIM3->ARR) result_pwm = TIM3->ARR;
	ramRomData[M4_POWER] = result_pwm;



}//void speed_task(void)


void mode_task(void)
{
	if (ramRomData[M1_GOAL_SPEED] > 0) DIR1_GPIO_Port->BRR=DIR1_Pin;
	else if (ramRomData[M1_GOAL_SPEED] < 0) DIR1_GPIO_Port->BSRR=DIR1_Pin;
	else ramRomData[M1_POWER]=0;

	if (ramRomData[M2_GOAL_SPEED] > 0) DIR2_GPIO_Port->BRR=DIR2_Pin;
	else if (ramRomData[M2_GOAL_SPEED] < 0) DIR2_GPIO_Port->BSRR=DIR2_Pin;
	else ramRomData[M2_POWER]=0;

	if (ramRomData[M3_GOAL_SPEED] > 0) DIR3_GPIO_Port->BRR=DIR3_Pin;
	else if (ramRomData[M3_GOAL_SPEED] < 0) DIR3_GPIO_Port->BSRR=DIR3_Pin;
	else ramRomData[M3_POWER]=0;

	if (ramRomData[M4_GOAL_SPEED] > 0) DIR4_GPIO_Port->BSRR=DIR4_Pin; //тут инверсия )))
	else if (ramRomData[M4_GOAL_SPEED] < 0) DIR4_GPIO_Port->BRR=DIR4_Pin;
	else ramRomData[M4_POWER]=0;

	if (ramRomData[MOTOR_MODE] == 0)
	{
		ramRomData[M1_POWER]=0;
		ramRomData[M2_POWER]=0;
		ramRomData[M3_POWER]=0;
		ramRomData[M4_POWER]=0;
		ramRomData[M1_GOAL_SPEED] = 0;
		ramRomData[M2_GOAL_SPEED] = 0;
		ramRomData[M3_GOAL_SPEED] = 0;
		ramRomData[M4_GOAL_SPEED] = 0;
		pwm_mode_task();
	}


}

void driver_task(void)
{
	if(flag_tim)
	{
		encoder_update();
		if(ramRomData[MOTOR_MODE])
		{

			if(ramRomData[MOTOR_MODE] == MOTOR_MODE_SPEED)
			{
				speed_task();
			}//if(ramRomData[MOTOR_MODE] == MOTOR_MODE_SPEED)
			pwm_mode_task();
			//TIM6->ARR = 10000/ramRomData[SPEED_PERIOD];

		}//if(ramRomData[MOTOR_MODE])
		flag_tim = 0;
		tiktik++;

	}//if(flag_tim)

}//void driver_task(void)



