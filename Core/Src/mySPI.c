/*
 * mySPI.c
 *
 *  Created on: 16 февр. 2022 г.
 *      Author: User
 */

#include "main.h"
#include <string.h>
extern uint8_t SPI_RxBufLastCNDTR;

uint8_t bufSPI_Tx[BUFSIZE], bufSPI_Rx[BUFSIZE];

uint16_t SPI_RXBUF_GetData(uint16_t *shift)
{
	uint32_t cndrt = DMA1_Channel2->CNDTR;
  *shift =  BUFSIZE - SPI_RxBufLastCNDTR;

  if(SPI_RxBufLastCNDTR > cndrt)
  {
    uint16_t len = SPI_RxBufLastCNDTR - cndrt;
    SPI_RxBufLastCNDTR = cndrt;
		return len;
  }
	else if(SPI_RxBufLastCNDTR < cndrt)
  {
    uint16_t len = SPI_RxBufLastCNDTR;
    SPI_RxBufLastCNDTR = BUFSIZE;
		return len;
  }
	return 0;
}
//----------------------------------------------
void SPI_TXBUF_PutData(uint8_t *buf, uint8_t dlina)
{
	if(dlina > BUFSIZE) return;

	uint8_t shift = BUFSIZE - DMA1_Channel3->CNDTR;

	if(dlina <= DMA1_Channel3->CNDTR)
	{
		memcpy(bufSPI_Tx+shift, buf, dlina);
	}
	else
	{
		uint8_t ostatok = dlina - BUFSIZE;
		memcpy(bufSPI_Tx+shift, buf, BUFSIZE);
		memcpy(bufSPI_Tx, buf + BUFSIZE, ostatok);
	}
}
