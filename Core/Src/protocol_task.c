/*
 * protocol_task.c
 *
 *  Created on: Feb 15, 2022
 *      Author: User
 */
#include "main.h"
#include "UART.h"
#include "mySPI.h"
#include "command_task.h"
#include <string.h>

extern uint8_t bufUART_Rx[];
extern uint8_t bufSPI_Rx[];
extern uint8_t bufCmd[], bufCmdCnt; // Буфер принятой команды и счётчик принятых байт. Заполнение буфера начинается после обнаружения маркера
extern uint8_t bufCmdSPI[], bufCmdCntSPI; // Буфер принятой команды и счётчик принятых байт. Заполнение буфера начинается после обнаружения маркера
//-------------------------------------------------------------
void protocol_task(void) {
	uint16_t n, shift;

	n = UART_RXBUF_GetData(&shift); // Проверка на поступление команды по rs-485
	if (n)
	{

		for (int i = 0; i < n; i++) { // Копирование данных в буфер команды с поиском маркера
			if (bufCmdCnt == 0)
			{
				uint8_t wrk = bufUART_Rx[i + shift];
				if (wrk == MARKER) { // Начало команды
					bufCmd[bufCmdCnt++] = wrk;
				}
			} else { // Заполнение буфера команды с проверкой принятого количества байт
				bufCmd[bufCmdCnt++] = bufUART_Rx[i + shift];

				if ((bufCmd[1] + 4) == bufCmdCnt) {
					command_task(bufCmd, CMDUART);
					memset(bufCmd, 0, BUFSIZE);
					bufCmdCnt = 0;
				}
			}
		} //for (int i = 0; i < n; i++)
	}

	else
		n = SPI_RXBUF_GetData(&shift); // Проверка на поступление команды по SPI
	if (n) {
		for (int i = 0; i < n; i++) { // Копирование данных в буфер команды с поиском маркера
			if (bufCmdCntSPI == 0) {
				uint8_t wrk = bufSPI_Rx[i + shift];
				if (wrk == MARKER) { // Начало команды
					bufCmdCntSPI = 0;
					bufCmdSPI[bufCmdCntSPI++] = wrk;
				}
			} else { // Заполнение буфера команды с проверкой принятого количества байт
				bufCmd[bufCmdCntSPI++] = bufSPI_Rx[i + shift];
				if ((bufCmdSPI[1] + 4) == bufCmdCntSPI) {
					command_task(bufCmdSPI, CMDSPI);
					memset(bufCmdSPI, 0, BUFSIZE);
					bufCmdCntSPI = 0;
				}
			}
		} // for (int i = 0; i < n; i++)
	}
}
