################################################################################
# Automatically-generated file. Do not edit!
# Toolchain: GNU Tools for STM32 (9-2020-q2-update)
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Core/Src/SPI1.c \
../Core/Src/UART.c \
../Core/Src/adc.c \
../Core/Src/command_task.c \
../Core/Src/crc16.c \
../Core/Src/developer.c \
../Core/Src/dma.c \
../Core/Src/drive.c \
../Core/Src/gpio.c \
../Core/Src/lptim.c \
../Core/Src/main.c \
../Core/Src/mySPI.c \
../Core/Src/protocol_task.c \
../Core/Src/ram_rom_data.c \
../Core/Src/rs485.c \
../Core/Src/spi.c \
../Core/Src/stm32l0xx_hal_msp.c \
../Core/Src/stm32l0xx_it.c \
../Core/Src/syscalls.c \
../Core/Src/sysmem.c \
../Core/Src/system_stm32l0xx.c \
../Core/Src/tim.c \
../Core/Src/usart.c 

OBJS += \
./Core/Src/SPI1.o \
./Core/Src/UART.o \
./Core/Src/adc.o \
./Core/Src/command_task.o \
./Core/Src/crc16.o \
./Core/Src/developer.o \
./Core/Src/dma.o \
./Core/Src/drive.o \
./Core/Src/gpio.o \
./Core/Src/lptim.o \
./Core/Src/main.o \
./Core/Src/mySPI.o \
./Core/Src/protocol_task.o \
./Core/Src/ram_rom_data.o \
./Core/Src/rs485.o \
./Core/Src/spi.o \
./Core/Src/stm32l0xx_hal_msp.o \
./Core/Src/stm32l0xx_it.o \
./Core/Src/syscalls.o \
./Core/Src/sysmem.o \
./Core/Src/system_stm32l0xx.o \
./Core/Src/tim.o \
./Core/Src/usart.o 

C_DEPS += \
./Core/Src/SPI1.d \
./Core/Src/UART.d \
./Core/Src/adc.d \
./Core/Src/command_task.d \
./Core/Src/crc16.d \
./Core/Src/developer.d \
./Core/Src/dma.d \
./Core/Src/drive.d \
./Core/Src/gpio.d \
./Core/Src/lptim.d \
./Core/Src/main.d \
./Core/Src/mySPI.d \
./Core/Src/protocol_task.d \
./Core/Src/ram_rom_data.d \
./Core/Src/rs485.d \
./Core/Src/spi.d \
./Core/Src/stm32l0xx_hal_msp.d \
./Core/Src/stm32l0xx_it.d \
./Core/Src/syscalls.d \
./Core/Src/sysmem.d \
./Core/Src/system_stm32l0xx.d \
./Core/Src/tim.d \
./Core/Src/usart.d 


# Each subdirectory must supply rules for building sources it contributes
Core/Src/%.o: ../Core/Src/%.c Core/Src/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m0plus -std=gnu11 -DUSE_HAL_DRIVER -DSTM32L071xx -DUSE_FULL_LL_DRIVER -c -I../Core/Inc -I../Drivers/STM32L0xx_HAL_Driver/Inc -I../Drivers/STM32L0xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32L0xx/Include -I../Drivers/CMSIS/Include -Os -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" --specs=nano.specs -mfloat-abi=soft -mthumb -o "$@"

clean: clean-Core-2f-Src

clean-Core-2f-Src:
	-$(RM) ./Core/Src/SPI1.d ./Core/Src/SPI1.o ./Core/Src/UART.d ./Core/Src/UART.o ./Core/Src/adc.d ./Core/Src/adc.o ./Core/Src/command_task.d ./Core/Src/command_task.o ./Core/Src/crc16.d ./Core/Src/crc16.o ./Core/Src/developer.d ./Core/Src/developer.o ./Core/Src/dma.d ./Core/Src/dma.o ./Core/Src/drive.d ./Core/Src/drive.o ./Core/Src/gpio.d ./Core/Src/gpio.o ./Core/Src/lptim.d ./Core/Src/lptim.o ./Core/Src/main.d ./Core/Src/main.o ./Core/Src/mySPI.d ./Core/Src/mySPI.o ./Core/Src/protocol_task.d ./Core/Src/protocol_task.o ./Core/Src/ram_rom_data.d ./Core/Src/ram_rom_data.o ./Core/Src/rs485.d ./Core/Src/rs485.o ./Core/Src/spi.d ./Core/Src/spi.o ./Core/Src/stm32l0xx_hal_msp.d ./Core/Src/stm32l0xx_hal_msp.o ./Core/Src/stm32l0xx_it.d ./Core/Src/stm32l0xx_it.o ./Core/Src/syscalls.d ./Core/Src/syscalls.o ./Core/Src/sysmem.d ./Core/Src/sysmem.o ./Core/Src/system_stm32l0xx.d ./Core/Src/system_stm32l0xx.o ./Core/Src/tim.d ./Core/Src/tim.o ./Core/Src/usart.d ./Core/Src/usart.o

.PHONY: clean-Core-2f-Src

